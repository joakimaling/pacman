program Pacman;

uses
	Allegro5,
	Level;

var
	Display: ALLEGRO_DISPLAYptr;
	Event: ALLEGRO_EVENT;
	Timer: ALLEGRO_TIMERptr;
	Queue: ALLEGRO_EVENT_QUEUEptr;
	ReDraw: Boolean;
	Level: TLevel;

procedure Draw;
begin
	al_clear_to_color(al_map_rgb(0, 0, 0));
	al_flip_display();
end;

begin
	if al_init then begin
		al_install_keyboard();
		Timer := al_create_timer(1.0 / 60.0);

		if Timer <> nil then begin
			Display := al_create_display(480, 320);

			if Display <> nil then begin
				Queue := al_create_event_queue();

				if Queue <> nil then begin
					al_register_event_source(Queue, al_get_display_event_source(Display));
					al_register_event_source(Queue, al_get_timer_event_source(Timer));
					al_register_event_source(Queue, al_get_keyboard_event_source());
					al_start_timer(Timer);
					Draw;

					while true do begin
						al_wait_for_event(Queue, Event);

						case Event._type of
							ALLEGRO_EVENT_TIMER:
								ReDraw := true;
							ALLEGRO_EVENT_DISPLAY_CLOSE:
								Break;
							ALLEGRO_EVENT_KEY_DOWN:
								WriteLn('KeyCode: ', Event.keyboard.keycode);
						end;

						if ReDraw and al_is_event_queue_empty(Queue) then begin
							ReDraw := false;
							Draw;
						end;
					end;

					al_destroy_event_queue(Queue);
				end;
				al_destroy_display(Display);
			end;
			al_destroy_timer(Timer);
		end;
		al_uninstall_system();
	end;
end.
