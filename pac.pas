program PacManiac;

uses
  Crt,Dos,Stack2Int;

const
  MAX_X=53;MAX_Y=18;MAX_G=3;
  POWER='@';FOOD='.';
  MAZE_COLOR=black;WALL_COLOR=darkgray;POWER_COLOR=red;FOOD_COLOR=white;
  ghost_color:array[1..4] of integer=(red,cyan,brown,white);

  {C_BOARD:array[1..MAX_X,1..MAX_Y] of char=
  (('0','4','4','4','4',' ','4','4','4','4','1'),
   ('5','.','@','.','.','.','.','.','@','.','5'),
   ('5','.','0','4','4','.','4','4','1','.','5'),
   ('5','.','5','.','.','.','.','.','5','.','5'),
   ('5','.','.','.','5','.','5','.','.','.','5'),
   ('9','4','4','4','8','.','5','.','4','4','8'),
   ('5','.','.','.','5','.','5','.','.','.','5'),
   ('5','.','5','.','.','.','.','.','5','.','5'),
   ('5','.','5','.','0','4','1','.','5','.','5'),
   ('5','.','5','.','5','G','5','.','5','.','5'),
   ('5','.','5','.',' ','G','5','.','5','P','5'),
   ('5','.','5','.','5','G','5','.','5','.','5'),
   ('5','.','5','.','2','4','3','.','5','.','5'),
   ('5','.','5','.','.','.','.','.','5','.','5'),
   ('5','.','.','.','5','.','5','.','.','.','5'),
   ('9','4','4','4','8','.','5','.','4','4','8'),
   ('5','.','.','.','5','.','5','.','.','.','5'),
   ('5','.','5','.','.','.','.','.','5','.','5'),
   ('5','.','2','4','4','.','4','4','3','.','5'),
   ('5','.','@','.','.','.','.','.','@','.','5'),
   ('2','4','4','4','4',' ','4','4','4','4','3'));}

type
  r_board=record
    obstacle:boolean;
    color:integer;
    thing:char;
  end;
  r_gubbe=record
    color,x,y:integer;
    weak:boolean;
    looks:char;
  end;
  v_board=array[1..MAX_X,1..MAX_Y] of r_board;
  v_ghost=array[1..MAX_G] of r_gubbe;

var
  visited:array[1..MAX_X,1..MAX_Y] of integer;
  pacman:r_gubbe;
  ghost:v_ghost;
  board:v_board;
  points,count:integer;
  quit:boolean;

procedure LoadMaze(filename:string);
  var 
    f:text;
    part:char;
	x,y:integer;
  begin
    Assign(f,filename);
	{$I-}
	Reset(f);
	if IOResult=0 then begin
	  
	  for y:=1 to MAX_X do begin
	    for x:=1 to MAX_Y do begin
	      if eof(f) then begin 
		  LoadMaze:=false;
	  	  break;
		Read(f,part);
		board[x,y]:=part;
	  end;
	  
	end;
	{$I+}
	
	Close(f);
  end;
  
procedure WriteXY(x,y,c:integer;txt:string);
  var temp:integer;
  begin
    temp:=TextAttr;
    TextAttr:=c;
    GotoXY(x,y);
    Write(txt);
    TextAttr:=temp;
  end;

procedure DrawScore;
  var score:string;
  begin
    Str(points,score);
    WriteXY(25,2,lightgray,'Score: '+score);
  end;

procedure InitGame;
  var i,x,y:integer;
  begin
    i:=1;
    ClrScr;
    CursorOff;
    points:=0;
    count:=0;
    quit:=false;
    Randomize;
    for x:=1 to MAX_X do begin
      for y:=1 to MAX_Y do begin
        board[x,y].thing:=C_BOARD[x,y];
        board[x,y].obstacle:=false;
        if C_BOARD[x,y] in ['0'..'9'] then begin
          case C_BOARD[x,y] of
            '0':board[x,y].thing:=#201;
            '1':board[x,y].thing:=#200;
            '2':board[x,y].thing:=#187;
            '3':board[x,y].thing:=#188;
            '4':board[x,y].thing:=#186;
            '5':board[x,y].thing:=#205;
            '6':board[x,y].thing:=#204;
            '7':board[x,y].thing:=#185;
            '8':board[x,y].thing:=#202;
            '9':board[x,y].thing:=#203
          end;
          board[x,y].obstacle:=true;
          board[x,y].color:=WALL_COLOR;
        end;
        if C_BOARD[x,y]=POWER then board[x,y].color:=POWER_COLOR;
        if C_BOARD[x,y]=FOOD then board[x,y].color:=FOOD_COLOR;
        if C_BOARD[x,y]='G' then begin
       	  ghost[i].looks:=#30;
          ghost[i].color:=ghost_color[i];
          ghost[i].weak:=false;
          ghost[i].x:=x;
          ghost[i].y:=y;
      	  inc(i);
      	end;
       	if C_BOARD[x,y]='P' then begin
          pacman.looks:=#1;
       	  pacman.color:=yellow;
          pacman.x:=x;
          pacman.y:=y;
      	end;
      end;
    end;
  end;

procedure DrawBoard;
  var i,x,y:integer;
  begin
    for x:=1 to MAX_X do begin
      for y:=1 to MAX_Y do begin
        WriteXY(x,y,board[x,y].color,board[x,y].thing);
      end;
    end;
    for i:=1 to MAX_G do begin
      WriteXY(ghost[i].x,ghost[i].y,ghost[i].color,ghost[i].looks);
    end;
    WriteXY(pacman.x,pacman.y,pacman.color,pacman.looks);
    DrawScore;
  end;

{returns the direction in which you should move from (startx, starty) to get to (endx, endy) quickest}
{onex ... foury are used for setting up the four stacks and thus control which directions are prioritised}
{of nx and ny, one must be 0 as diagonals aren't allowed.  the other may be -1 or +1, nothing else will work right}
{as we only want to start our explore from one square away from the enemy}
function FindPath(sx,sy,ex,ey,x1,y1,x2,y2,x3,y3,x4,y4:integer):integer;
  var
    stack,newstack:array[1..4] of nodeptr;{up, down, left, right}
    activestacks,currentstack,winningstacknumber,i,j:integer;
	{STACKACTIVE is used to determine when only one way is still moving and so we can stop, or when nothing is moving}
    {CURRENTSTACK is used to determine which is the only moving stack if only one is moving}
    addedtoactivestacks,finished:boolean;
  begin
    {set up stacks}
    if not board[sx+x1,sy+y1].obstacle then stack[1]:=MakeNode(sx+x1,sy+y1) else stack[1]:=nil;
    if not board[sx+x2,sy+y2].obstacle then stack[2]:=MakeNode(sx+x2,sy+y2) else stack[2]:=nil;
    if not board[sx+x3,sy+y3].obstacle then stack[3]:=MakeNode(sx+x3,sy+y3) else stack[3]:=nil;
    if not board[sx+x4,sy+y4].obstacle then stack[4]:=MakeNode(sx+x4,sy+y4) else stack[4]:=nil;
    for i:=1 to 4 do newstack[i]:=nil;{reset everything}
    finished:=false;
    while not finished do begin
      {set up monitoring variables}
      currentstack:=0;
      activestacks:=0;
      {do stuff for each stack}
	  for i:=1 to 4 do begin
        {stack1}
		addedtoactivestacks:=false;
        while (stack[i]<>nil) and (not finished) do begin
          currentstack:=i;
          if not addedtoactivestacks then begin
            Inc(activestacks);
            addedtoactivestacks:=true;
          end;
          if (stack[i]^.x=ex) and (stack[i]^.y=ey) then begin
            for j:=1 to 4 do stack[j]:=Clear(stack[j]);
            finished:=true;
            winningstacknumber:=i; {set up return value}
          end;
          {try all directions}
          if not finished then begin
            if ((not board[stack[i]^.x,stack[i]^.y-1].obstacle) and (visited[stack[i]^.x,stack[i]^.y-1]=0)
            and ((stack[i]^.x<>sx) or (stack[i]^.y-1<>sy))) then begin
              newstack[i]:=Push(newstack[i],MakeNode(stack[i]^.x,stack[i]^.y-1));
              visited[stack[i]^.x,stack[i]^.y-1]:=i;
            end;
            if ((not board[stack[i]^.x,stack[i]^.y+1].obstacle) and (visited[stack[i]^.x,stack[i]^.y+1]=0)
            and ((stack[i]^.x<>sx) or (stack[i]^.y+1<>sy))) then begin
              newstack[i]:=Push(newstack[i],MakeNode(stack[i]^.x,stack[i]^.y+1));
              visited[stack[i]^.x,stack[i]^.y+1]:=i;
            end;
            if ((not board[stack[i]^.x-1,stack[i]^.y].obstacle) and (visited[stack[i]^.x-1,stack[i]^.y]=0)
            and ((stack[i]^.x-1<>sx) or (stack[i]^.y<>sy))) then begin
              newstack[i]:=Push(newstack[i],MakeNode(stack[i]^.x-1,stack[i]^.y));
              visited[stack[i]^.x-1,stack[i]^.y]:=i;
            end;
            if ((not board[stack[i]^.x+1,stack[i]^.y].obstacle) and (visited[stack[i]^.x+1,stack[i]^.y]=0)
            and ((stack[i]^.x+1<>sx) or (stack[i]^.y<>sy))) then begin
               newstack[i]:=Push(newstack[i],MakeNode(stack[i]^.x+1,stack[i]^.y));
               visited[stack[i]^.x+1,stack[i]^.y]:=i;
            end;
            stack[i]:=Pop(stack[i]);
          end;
        end;
        stack[i]:=newstack[i];
        newstack[i]:=nil;
	  end;
      {optimisation}
      if activestacks<2 then begin
        finished:=true;
        winningstacknumber:=currentstack;
      end;
      {returns 1 = up, 2 = down, 3 = left, 4 = right}
      {must therefore firgure out which stack is which direction as it's not necessarily stack 1 = up anymore}
      {because of the fact we can alter priorities}
      case winningstacknumber of
	    1:if x1=1 then FindPath:=4 else if x1=-1 then FindPath:=3 else if y1=1 then FindPath:=2 else if y1=-1 then FindPath:=1;
	    2:if x2=1 then FindPath:=4 else if x2=-1 then FindPath:=3 else if y2=1 then FindPath:=2 else if y2=-1 then FindPath:=1;
	    3:if x3=1 then FindPath:=4 else if x3=-1 then FindPath:=3 else if y3=1 then FindPath:=2 else if y3=-1 then FindPath:=1;
	    4:if x4=1 then FindPath:=4 else if x4=-1 then FindPath:=3 else if y4=1 then FindPath:=2 else if y4=-1 then FindPath:=1;
      end;
    end;
  end;


procedure GhostMove(x,y:integer;ghost:r_gubbe);
  begin
    if not board[ghost.x+x,ghost.y+y].obstacle then begin
      GotoXY(ghost.x,ghost.y);
      TextAttr:=board[ghost.x,ghost.y].color;
      Write(board[ghost.x,ghost.y].thing);
      ghost.x:=ghost.x+x;
      ghost.y:=ghost.y+x;
      GotoXY(ghost.x,ghost.y);
      TextAttr:=ghost.color;
      Write(ghost.looks);
    end;
  end;

procedure MoveGhosts;
  const
    priority:array[1..24] of string[4]=('UDLR','DULR','LDUR','RULD','ULRD','DLRU','LURD','RDLU','URLD','DRLU',
	'LRUD','RLUD','UDRL','DURL','LDRU','RDUL','ULDR','DLUR','LUDR','RLDU','URDL','DRUL','LRDU','RUDL');
  var
    choice,priorityset,direction,temp,i,x,y:integer;
    prioritysend:array[1..2,1..4] of integer;
  begin
    for i:=1 to MAX_G do begin
      for x:=1 to 40 do for y:=1 to 15 do visited[x,y]:=0;{Clear the visited spots}
      choice:=0;
      priorityset:=1;
      if priorityset>24 then priorityset:=1 else Inc(priorityset);
      temp:=Random(3);
      if temp=0 then begin
        for direction:=1 to 4 do begin
          case priority[priorityset][direction] of
            'U':begin
                  prioritysend[1,direction]:=0;
                  prioritysend[2,direction]:=-1;
                end;
            'D':begin
                  prioritysend[1,direction]:=0;
                  prioritysend[2,direction]:=1;
                end;
            'L':begin
                  prioritysend[1,direction]:=-1;
                  prioritysend[2,direction]:=0;
                end;
            'R':begin
                  prioritysend[1,direction]:=1;
                  prioritysend[2,direction]:=0;
                end;
          end;
        end;
        choice:=FindPath(ghost[i].x,ghost[i].y,pacman.x,pacman.y,
        prioritysend[1][1],prioritysend[2][1],prioritysend[1][2],prioritysend[2][2],
        prioritysend[1][3],prioritysend[2][3],prioritysend[1][4],prioritysend[2][4]);
      end;
      case choice of
        1:GhostMove(0,-1,ghost[i]);
        2:GhostMove(0,1,ghost[i]);
        3:GhostMove(-1,0,ghost[i]);
        4:GhostMove(1,0,ghost[i]);
      end;
    end;
  end;

procedure MovePacman(x,y:integer);
  var i:integer;
  begin
    if not board[pacman.x+x,pacman.y+y].obstacle then begin
      if board[pacman.x+x,pacman.y+y].thing=FOOD then inc(points);
      if board[pacman.x+x,pacman.y+y].thing=POWER then
        for i:=1 to MAX_G do begin
          ghost[i].weak:=true;
          ghost[i].color:=cyan;
        end;
      GotoXY(pacman.x,pacman.y);
      board[pacman.x,pacman.y].thing:=#32;
      Write(#32);//radera p� aktuell position
      if (pacman.x=1) and (x=-1) then pacman.x:=MAX_X	  //Om pacman �r p� ena �nden f�rs han till den andra
      else if (pacman.x=MAX_X) and (x=1) then pacman.x:=1 //Om pacman �r p� andra �nden f�rs han till den ena
      else pacman.x:=pacman.x+x;
      if (pacman.y=1) and (y=-1) then pacman.y:=MAX_Y 	  //Precis likadant vertikalt!
      else if (pacman.y=MAX_Y) and (y=1) then pacman.y:=1
      else pacman.y:=pacman.y+y;
      WriteXY(pacman.x,pacman.y,pacman.color,pacman.looks);
      DrawScore;
    end;
  end;

procedure KeyboardAction;
  begin
    case Ord(ReadKey) of
      72:MovePacman(0,-1);
      80:MovePacman(0,1);
      75:MovePacman(-1,0);
      77:MovePacman(1,0);
      27:begin
           quit:=true;
      	   CursorOn;
         end;
    end;
  end;

begin
  InitGame;
  DrawBoard;
  repeat
    repeat
      //MoveGhosts;
    until KeyPressed;
    KeyboardAction;
  until quit;
end.
