FPC=fpc
CFLAGS=-Fuallegro.pas/lib -Fucore -Fuentities -Fughosts -Futiles -MObjfpc

.PHONY: all clean

all:
	$(FPC) $(CFLAGS) Pacman.pas

clean:
	$(RM) Pacman *.o *.ppu
