unit Floor;

interface

uses
	Entity, Tile;

type
	TFloor = class(TTile)
		public
			constructor Create;
			function IsPenetrable: Boolean;
		private
			FContainer: TEntity; // Either Food or PowerUp
	end;

implementation

constructor TFloor.Create;
begin
	inherited Create(' ');
end;

function TFloor.IsPenetrable: Boolean;
begin
	IsPenetrable := true;
end;

end.
