unit Wall;

interface

uses
	Tile;

type
	TWall = class(TTile)
		public
			constructor Create;
			function IsPenetrable: Boolean;
	end;

implementation

constructor TWall.Create;
begin
	inherited Create('+');
end;

function TWall.IsPenetrable: Boolean;
begin
	IsPenetrable := false;
end;

end.
