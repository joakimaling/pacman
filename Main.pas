program PacmanGame;

uses
	ConsoleBuffer,
	Keyboard;

const
	Faces: Array[0..3] of Char = ('E', 'N', 'S', 'W');
	Map: Array[0..30] of String = (
		'############################',
		'#************##************#',
		'#*####*#####*##*#####*####*#',
		'#@####*#####*##*#####*####@#',
		'#*####*#####*##*#####*####*#',
		'#**************************#',
		'#*####*##*########*##*####*#',
		'#*####*##*########*##*####*#',
		'#******##****##****##******#',
		'######*#####*##*#####*######',
		'     #*#####*##*#####*#     ',
		'     #*##**********##*#     ',
		'     #*##*###--###*##*#     ',
		'######*##*#      #*##*######',
		'      ****#      #****      ',
		'######*##*#      #*##*######',
		'     #*##*########*##*#     ',
		'     #*##**********##*#     ',
		'     #*##*########*##*#     ',
		'######*##*########*##*######',
		'#************##************#',
		'#*####*#####*##*#####*####*#',
		'#@####*#####*##*#####*####@#',
		'#***##*******  *******##***#',
		'###*##*##*########*##*##*###',
		'###*##*##*########*##*##*###',
		'#******##****##****##******#',
		'#*##########*##*##########*#',
		'#*##########*##*##########*#',
		'#**************************#',
		'############################'
	);

type
	Directions = (East, North, South, West);
	Status = (Eaten, Chasing, Frightened, Scattering);
	Entity = record
		Direction: Directions;
		X, Y: Byte;
	end;

var
	Hue: Word;
	KeyEvent: TKeyEvent;
	Pacman: Entity;
	Quit: Boolean = false;
	X, Y: Byte;

	Blinky, Inky, Pinky, Clyde: Entity;
	BlinkyTarget, InkyTarget, PinkyTarget, ClydeTarget: Word;

begin
	InitKeyboard;
	BufferStart;
	Pacman.Direction := West;
	Pacman.X := 14;
	Pacman.Y := 23;

	Blinky.Direction := East;
	Blinky.X := 14;
	Blinky.Y := 11;
	BlinkyTarget := $1800;

	Inky.Direction := North;
	Inky.X := 12;
	Inky.Y := 14;
	InkyTarget := $1A1D;

	Pinky.Direction := South;
	Pinky.X := 14;
	Pinky.Y := 14;
	PinkyTarget := $0400;

	Clyde.Direction := North;
	Clyde.X := 16;
	Clyde.Y := 14;
	ClydeTarget := $011D;

	repeat
		for Y := 0 to High(Map) do begin
			for X := 0 to High(Map[Y]) do begin
				if Map[Y][X] = '#' then Hue := $1015 else Hue := $10FF;
				BufferDrawText(X, Y, Map[Y][X], Hue);
			end;
		end;
		if KeyPressed then begin
			KeyEvent := TranslateKeyEvent(GetKeyEvent);
			case GetKeyEventCode(KeyEvent) of
				$FF21: Pacman.Direction := North;
				$FF23: Pacman.Direction := East;
				$FF25: Pacman.Direction := West;
				$FF27: Pacman.Direction := South;
				else Quit := true;
			end;
		end;

		// Move Pacman
		case Pacman.Direction of
			East: if (Pacman.X > 0) and (Map[Pacman.Y][Pacman.X - 1] <> '#') then Dec(Pacman.X);
			North: if (Pacman.Y > 0) and (Map[Pacman.Y - 1][Pacman.X] <> '#') then Dec(Pacman.Y);
			South: if (Pacman.Y < 31) and (Map[Pacman.Y + 1][Pacman.X] <> '#') then Inc(Pacman.Y);
			West: if (Pacman.X < 28) and (Map[Pacman.Y][Pacman.X + 1] <> '#') then Inc(Pacman.X);
		end;

		// Move Blinky

		// Move Inky

		// Move Pinky

		// Move Clyde


		BufferSetPixel(Pacman.X, Pacman.Y, Faces[Byte(Pacman.Direction)], $10E2);

		BufferSetPixel(Blinky.X, Blinky.Y, Faces[Byte(Blinky.Direction)], $10C4);
		BufferSetPixel(Inky.X, Inky.Y, Faces[Byte(Inky.Direction)], $1033);
		BufferSetPixel(Pinky.X, Pinky.Y, Faces[Byte(Pinky.Direction)], $10DB);
		BufferSetPixel(Clyde.X, Clyde.Y, Faces[Byte(Clyde.Direction)], $10D0);

		BufferSetPixel(Hi(BlinkyTarget), Lo(BlinkyTarget), 'X', $10C4);
		BufferSetPixel(Hi(InkyTarget), Lo(InkyTarget), 'X', $1033);
		BufferSetPixel(Hi(PinkyTarget), Lo(PinkyTarget), 'X', $10DB);
		BufferSetPixel(Hi(ClydeTarget), Lo(ClydeTarget), 'X', $10D0);

		BufferDraw;
	until Quit;
	BufferStop;
	DoneKeyboard;
end.
