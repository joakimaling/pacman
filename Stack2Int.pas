{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator.... Joakim Åling, Sweden                                                                 }
{ Date....... January 25, 2009                                                                     }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
unit Stack2Int;

interface
{--------------------------------------------------------------------------------------------------}
type
	PNode = ^TNode;
	TNode = record
		X, Y: Integer;
		Next: PNode;
	end;
{--------------------------------------------------------------------------------------------------}
function Push(Stack, NewNode: PNode): PNode;
{}
{--------------------------------------------------------------------------------------------------}
function Pop(Stack: PNode): PNode;
{returns the smaller stack, not the popped item}
{--------------------------------------------------------------------------------------------------}
function MakeNode(X, Y: Integer): PNode;
{}
{--------------------------------------------------------------------------------------------------}
function Clear(Stack: PNode): PNode;
{}
{--------------------------------------------------------------------------------------------------}
implementation
{--------------------------------------------------------------------------------------------------}
function Push(Stack, NewNode: PNode): PNode;
begin
	NewNode^.Next := Stack;
	Push := NewNode;
end;
{--------------------------------------------------------------------------------------------------}
function Pop(Stack: PNode): PNode; {returns the smaller stack, not the popped item}
var
	SmallerStack: PNode;
begin
	SmallerStack := Stack^.Next; {make the second item the top of the stack}
	Dispose(Stack);
	Pop := SmallerStack;
end;
{--------------------------------------------------------------------------------------------------}
function MakeNode(X, Y: Integer): PNode;
var
	NewNode: PNode;
begin
	New(NewNode); {grab memory for the node and make newnode point to it}
	NewNode^.X := X;
	NewNode^.X := Y;
	NewNode^.Next := nil;
	MakeNode := NewNode;
end;
{--------------------------------------------------------------------------------------------------}
function Clear(Stack: PNode): PNode;
begin
	while Stack <> nil do Stack := Pop(Stack);
	Clear := Stack;
end;
{--------------------------------------------------------------------------------------------------}
initialization

finalization

end.