unit Tile;

interface

uses
	Entity;

type
	TTile = class(TEntity)
		public
			constructor Create(Symbol: Char);
			function IsPenetrable: Boolean; abstract;
	end;

implementation

constructor TTile.Create(Symbol: Char);
begin
	inherited Create(Symbol);
end;

end.
