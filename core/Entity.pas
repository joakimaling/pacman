unit Entity;

interface

type
	TEntity = class
		public
			constructor Create(Symbol: Char);
			procedure Draw;
		private
			FSymbol: Char;
	end;

implementation

constructor TEntity.Create(Symbol: Char);
begin
	FSymbol := Symbol;
end;

procedure TEntity.Draw;
begin
	Write(FSymbol);
end;

end.
