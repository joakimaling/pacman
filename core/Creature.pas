unit Creature;

interface

uses
	Entity;

type
	TCreature = class(TEntity)
		public
			constructor Create(Symbol: Char);
			procedure Tick; abstract;
		protected
			FX, FY: Integer;
	end;

implementation

constructor TCreature.Create(Symbol: Char);
begin
	inherited Create(Symbol);
end;

end.
