unit Ghost;

interface

uses
	Creature;

type
	TGhost = class(TCreature)
		private
			FIsWeak: Boolean;
		public
			constructor Create(Symbol: Char);
			procedure Tick;
			property IsWeak: Boolean read FIsWeak;
	end;

implementation

constructor TGhost.Create(Symbol: Char);
begin
	inherited Create(Symbol);
	FIsWeak := false;
end;

procedure TGhost.Tick;
begin

end;

end.
