unit Pacman;

interface

uses
	Creature;

type
	TPacman = class(TCreature)
		public
			constructor Create;
			procedure Tick;
		private
			FScore: LongInt;
	end;

implementation

constructor TPacman.Create;
begin
	inherited Create('C');
end;

procedure TPacman.Tick;
begin

end;

end.
