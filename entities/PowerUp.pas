unit PowerUp;

interface

uses
	Entity;

type
	TPowerUp = class(TEntity)
		public
			constructor Create;
	end;

implementation

constructor TPowerUp.Create;
begin
	inherited Create('@');
end;

end.
