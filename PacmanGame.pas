{--------------------------------------------------------------------------------------------------}
{                                                                                                  }
{ Creator....  Joakim Åling, Sweden                                                                }
{ Date.......  January 25, 2009                                                                    }
{ Website....                                                                                      }
{                                                                                                  }
{ Description                                                                                      }
{                                                                                                  }
{                                                                                                  }
{ Usage......                                                                                      }
{                                                                                                  }
{ History....                                                                                      }
{                                                                                                  }
{ Information                                                                                      }
{                                                                                                  }
{--------------------------------------------------------------------------------------------------}
program PacmanGame;

{$mode objfpc}
{$M+}

uses
	Crt, SysUtils, Stack2Int;
{--------------------------------------------------------------------------------------------------}
const
	GhostAttr: array[0..3] of Byte = ($03, $04, $05, $06);
	Obstacles = ['|', '-', '+'];
	Power = '@';
	Food = '.';
{--------------------------------------------------------------------------------------------------}
type
	TBoard = array of array of record
		Face: Char;
		Attr: Byte;
	end;
	TGhost = record
		Weak: Boolean;
		Face: Char;
		X, Y, Attr: Byte;
	end;
{--------------------------------------------------------------------------------------------------}
var
	BoardWidth, BoardHeight: Byte;
	Board: TBoard;
	Ghost: array[0..3] of TGhost;
	Pacman: TGhost;
	Visited: array[1..40, 1..15] of Integer;
	Score: Integer;
	Quit: Boolean;
{--------------------------------------------------------------------------------------------------}
procedure WriteXY(X, Y: Byte; Txt: String; Attr: Byte);
var
	OldAttr: Byte;
begin
	OldAttr := TextAttr;
	TextAttr := Attr;
	GotoXY(X, Y);
	Write(Txt);
	TextAttr := OldAttr;
end;
{--------------------------------------------------------------------------------------------------}
procedure InitBoard(FileName: String);
var
	UserFile: Text;
	Cell: Char;
	X, Y: Byte;
begin
	Assign(UserFile, FileName);
	{$I-}
	Reset(UserFile);
	if IOResult = 0 then begin
		Read(UserFile, BoardWidth, BoardHeight);
		SetLength(Board, BoardWidth);
		for X := 0 to BoardWidth - 1 do begin
			SetLength(Board[X], BoardHeight);
			for Y := 0 to BoardHeight - 1 do begin
				Read(UserFile, Cell);
				case Cell of
					Power, Food:
						begin
							Board[X, Y].Face := Cell;
							Board[X, Y].Attr := $07;
						end;
					'P':
						begin
							Board[X, Y].Face := #32;
							Pacman.X := X;
							Pacman.Y := Y;
						end;
					else
						begin
							Board[X, Y].Face := Cell;
							Board[X, Y].Attr := $01;
						end;
				end;
			end;
		end;
	end;
	{$I+}
	Close(UserFile);
end;
{--------------------------------------------------------------------------------------------------}
procedure InitGame;
var
	Index: Byte;
begin
	InitBoard('map1.txt');
	for Index := 0 to 3 do begin
		Ghost[Index].Attr := GhostAttr[Index];
		Ghost[Index].Face := #234;
		Ghost[Index].Weak := false;
	end;
	Pacman.Face := #67;
	Pacman.Attr := $0e;
	{Pacman.Weak := true;}
	Score := 0;
end;
{--------------------------------------------------------------------------------------------------}
procedure DrawBoard;
var
	X, Y: Byte;
begin
	for X := 0 to BoardWidth - 1 do begin
		for Y := 0 to BoardHeight - 1 do begin
			WriteXY(X, Y, Board[X, Y].Face, Board[X, Y].Attr);
		end;
	end;
	for X := 0 to 3 do begin
		WriteXY(Ghost[X].X, Ghost[X].Y, Ghost[X].Face, Ghost[X].Attr);
	end;
	WriteXY(Pacman.X, Pacman.Y, Pacman.Face, Pacman.Attr);
end;
{--------------------------------------------------------------------------------------------------}
function FindPath(SX, SY,EX, EY, X1, Y1, X2, Y2, X3, Y3, X4, Y4: Integer): Integer;
var
	Stack, NewStack: array[1..4] of PNode;
	ActiveStacks, CurrentStack, WinningStack, Index, Temp: Integer;
	AddedToActiveStacks, Finished: Boolean;
begin
	if not (Board[SX + X1, SY + Y1].Face in Obstacles) then Stack[1] := MakeNode(SX + X1, SY + Y1) else Stack[1] := nil;
	if not (Board[SX + X2, SY + Y2].Face in Obstacles) then Stack[2] := MakeNode(SX + X2, SY + Y2) else Stack[2] := nil;
	if not (Board[SX + X3, SY + Y3].Face in Obstacles) then Stack[3] := MakeNode(SX + X3, SY + Y3) else Stack[3] := nil;
	if not (Board[SX + X4, SY + Y4].Face in Obstacles) then Stack[4] := MakeNode(SX + X4, SY + Y4) else Stack[4] := nil;
	for Index := 1 to 4 do NewStack[Index] := nil;
	Finished := false;
	while not Finished do begin
		CurrentStack := 0;
		ActiveStacks := 0;
		for Index := 1 to 4 do begin
			AddedToActiveStacks := false;
			while (Stack[Index] <> nil) and (not Finished) do begin
				CurrentStack := Index;
				if not AddedToActiveStacks then begin
					Inc(ActiveStacks);
					AddedToActiveStacks := true;
				end;
				if (Stack[Index]^.X = EX) and (Stack[Index]^.Y = EY) then begin
					for Temp := 1 to 4 do Stack[Temp] := Clear(Stack[Temp]);
					Finished := true;
					WinningStack := Index;
				end;
				if not Finished then begin
					if not (Board[Stack[Index]^.X, Stack[Index]^.Y - 1].Face in Obstacles) and (Visited[Stack[Index]^.X, Stack[Index]^.Y - 1] = 0) and ((Stack[Index]^.X <> SX) or (Stack[Index]^.Y - 1 <> SY)) then begin
						NewStack[Index] := Push(NewStack[Index], MakeNode(Stack[Index]^.X, Stack[Index]^.Y - 1));
						Visited[Stack[Index]^.X, Stack[Index]^.Y - 1] := Index;
					end;
					if not (Board[Stack[Index]^.X, Stack[Index]^.Y + 1].Face in Obstacles) and (Visited[Stack[Index]^.X, Stack[Index]^.Y + 1] = 0) and ((Stack[Index]^.X <> SX) or (Stack[Index]^.Y + 1 <> SY)) then begin
						NewStack[Index] := Push(NewStack[Index], MakeNode(Stack[Index]^.X, Stack[Index]^.Y + 1));
						Visited[Stack[Index]^.X, Stack[Index]^.Y + 1] := Index;
					end;
					if not (Board[Stack[Index]^.X - 1, Stack[Index]^.Y].Face in Obstacles) and (Visited[Stack[Index]^.X - 1, Stack[Index]^.Y] = 0) and ((Stack[Index]^.X - 1 <> SX) or (Stack[Index]^.Y <> SY)) then begin
						NewStack[Index] := Push(NewStack[Index], MakeNode(Stack[Index]^.X - 1, Stack[Index]^.Y));
						Visited[Stack[Index]^.X - 1, Stack[Index]^.Y] := Index;
					end;
					if not (Board[Stack[Index]^.X + 1, Stack[Index]^.Y].Face in Obstacles) and (Visited[Stack[Index]^.X + 1, Stack[Index]^.Y] = 0) and ((Stack[Index]^.X + 1 <> SX) or (Stack[Index]^.Y <> SY)) then begin
						NewStack[Index] := Push(NewStack[Index], MakeNode(Stack[Index]^.X + 1, Stack[Index]^.Y));
						Visited[Stack[Index]^.X + 1, Stack[Index]^.Y] := Index;
					end;
					Stack[Index] := Pop(Stack[Index])
				end;
			end;
			Stack[Index] := NewStack[Index];
			NewStack[Index] := nil;
		end;
		if ActiveStacks < 2 then begin
			Finished := true;
			WinningStack := CurrentStack;
		end;
		case WinningStack of
			1:
				if X1 = 1 then FindPath := 4 else if X1 = -1 then FindPath := 3 else if Y1 = 1 then FindPath := 2 else if Y1 = -1 then FindPath := 1;
			2:
				if X1 = 1 then FindPath := 4 else if X1 = -1 then FindPath := 3 else if Y1 = 1 then FindPath := 2 else if Y1 = -1 then FindPath := 1;
			3:
				if X1 = 1 then FindPath := 4 else if X1 = -1 then FindPath := 3 else if Y1 = 1 then FindPath := 2 else if Y1 = -1 then FindPath := 1;			
			4:	
				if X1 = 1 then FindPath := 4 else if X1 = -1 then FindPath := 3 else if Y1 = 1 then FindPath := 2 else if Y1 = -1 then FindPath := 1;
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure MoveGhosts;
const
	Priority: array[1..24] of String[4] = ('UDLR', 'DULR', 'LDUR', 'RULD', 'ULRD', 'DLRU', 'LURD', 'RDLU', 'URLD', 'DRLU', 
'LRUD', 'RLUD', 'UDRL', 'DURL', 'LDRU', 'RDUL', 'ULDR', 'DLUR', 'LUDR' , 'RLDU', 'URDL', 'DRUL', 'LRDU', 'RUDL');
var
	Choice, PrioritySet, Direction, Index, Temp, X, Y: Integer;
	PrioritySend: array[1..2, 1..4] of Integer;
	
	procedure GhostMove(X, Y: Integer; Ghost: TGhost);
	begin
		if not (Board[Ghost.X + X, Ghost.Y + Y].Face in Obstacles) then begin
			GotoXY(Ghost.X, Ghost.Y);
			TextAttr := Board[Ghost.X, Ghost.Y].Attr;
			Write(Board[Ghost.X, Ghost.Y].Face);
			Ghost.X := Ghost.X + X;
			Ghost.Y := Ghost.Y + Y;
			GotoXY(Ghost.X, Ghost.Y);
			TextAttr := Ghost.Attr;
			Write(Ghost.Face);
		end;
	end;

begin
	for Index := 1 to 3 do begin
		for X := 1 to 40 do begin
			for Y := 1 to 15 do begin
				Visited[X, Y] := 0;
			end;
		end;
		Choice := 0;
		PrioritySet := 1;
		if PrioritySet > 24 then PrioritySet := 1 else Inc(PrioritySet);
		Temp := Random(3);
		if Temp = 0 then begin
			for Direction := 1 to 4 do begin
				case Priority[PrioritySet][Direction] of
					'U':
						begin
							PrioritySend[1, Direction] := 0;
							PrioritySend[2, Direction] := -1;
						end;
					'D':
						begin
							PrioritySend[1, Direction] := 0;
							PrioritySend[2, Direction] := 1;
						end;
					'L':
						begin
							PrioritySend[1, Direction] := -1;
							PrioritySend[2, Direction] := 0;
						end;
					'R':
						begin
							PrioritySend[1, Direction] := 1;
							PrioritySend[2, Direction] := 0;
						end;
				end;
			end;
			Choice := FindPath(Ghost[Index].X, Ghost[Index].Y, Pacman.X, Pacman.Y, 
				PrioritySend[1][1], PrioritySend[2][1], PrioritySend[1][2], PrioritySend[2][2],
				PrioritySend[1][3], PrioritySend[2][3], PrioritySend[1][4], PrioritySend[2][4]);
		end;
		case Choice of
			1:
				GhostMove(0, -1, Ghost[Index]);
			2:
				GhostMove(0, 1, Ghost[Index]);
			3:
				GhostMove(-1, 0, Ghost[Index]);
			4:
				GhostMove(1, 0, Ghost[Index]);
		end;
	end;
end;
{--------------------------------------------------------------------------------------------------}
procedure MovePacman(X, Y: Integer);
var
	Index: Byte;
begin
	if not (Board[Pacman.X + X, Pacman.Y + Y].Face in Obstacles) then begin
	
		if Board[Pacman.X + X, Pacman.Y + Y].Face = Food then Inc(Score);
		
		if Board[Pacman.X + X, Pacman.Y + Y].Face = Power then
			for Index := 0 to 3 do begin
				Ghost[Index].Weak := true;
			end;
		end;
			
		GotoXY(Pacman.X, Pacman.Y);
		Board[Pacman.X, Pacman.Y].Face := #32;
		Write(#32);

		if (Pacman.X = 0) and (X = -1) then begin 
			Pacman.X := BoardWidth; 
		end else if (Pacman.X = BoardWidth) and (X = 1) then begin
			Pacman.X := 1;
		end else begin
			Pacman.X := Pacman.X + X;

		if (Pacman.Y = 1) and (Y = -1) then begin
			Pacman.Y := BoardHeight;
		end else if (Pacman.Y = BoardHeight) and (Y = 1) then begin
			Pacman.Y := 1;
		end else begin
			Pacman.Y := Pacman.Y + Y;
		end;

		WriteXY(Pacman.X, Pacman.Y, Pacman.Face, Pacman.Attr);
	end;
end;
{--------------------------------------------------------------------------------------------------}
begin
	InitGame;
	repeat
		repeat
			MoveGhosts;
			Sleep(10);
		until KeyPressed;
		case ReadKey of
			#72:
				MovePacman(0, -1);
			#75:
				MovePacman(-1, 0);
			#77:
				MovePacman(1, 0);
			#80:
				MovePacman(0, 1);
			#27:
				Quit := true;
		end;
	until Quit;
end.
